# Requires gnome-dev and libwebkitgtk3-devel packages (opensuse)
webkit-01: webkit-01.c
	gcc webkit-01.c -o webkit-01 `pkg-config --cflags --libs webkitgtk-3.0`

webkit-02: webkit-02.c
	gcc webkit-02.c -o webkit-02 `pkg-config --cflags --libs webkitgtk-3.0`

# Requires libnotify-devel (opensuse)
webkit-03: webkit-03.c
	gcc -o webkit-03 webkit-03.c `pkg-config --cflags --libs webkitgtk-3.0 libnotify`

# Requires dbus-1-glib-devel
webkit-04: webkit-04.c
	gcc -o webkit-04 webkit-04.c `pkg-config --cflags --libs webkitgtk-3.0 dbus-glib-1`

webkit-05: webkit-05.c
	gcc -o webkit-05 webkit-05.c `pkg-config --cflags --libs webkitgtk-3.0 dbus-glib-1`

transparent: transparent.c
	gcc -o transparent transparent.c `pkg-config --cflags --libs webkitgtk-3.0`
